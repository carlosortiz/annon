package com.jmpeax.annon.config;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.Objects;
import java.util.Set;

public class AnnonConfig
{
    private Database database;
    private Set<Table> tables;

    public Database getDatabase()
    {
        return database;
    }

    public void setDatabase(final Database database)
    {
        this.database = database;
    }

    public Set<Table> getTables()
    {
        return tables;
    }

    public void setTables(final Set<Table> tables)
    {
        this.tables = tables;
    }

    @Override
    public boolean equals(final Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final AnnonConfig that = (AnnonConfig) o;
        return Objects.equals(database, that.database) &&
                Objects.equals(tables, that.tables);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(database, tables);
    }

    @Override
    public String toString()
    {
        return new ToStringBuilder(this)
                .append("database", database)
                .append("tables", tables)
                .toString();
    }
}
