package com.jmpeax.annon.config;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.Objects;

public class Column
{
    private String name;
    private String type;
    private String format;

    public String getName()
    {
        return name;
    }

    public void setName(final String name)
    {
        this.name = name;
    }

    public String getType()
    {
        return type;
    }

    public void setType(final String type)
    {
        this.type = type;
    }

    public String getFormat()
    {
        return format;
    }

    public void setFormat(final String format)
    {
        this.format = format;
    }

    @Override
    public boolean equals(final Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final Column column = (Column) o;
        return Objects.equals(name, column.name) &&
                Objects.equals(type, column.type) &&
                Objects.equals(format, column.format);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(name, type, format);
    }

    @Override
    public String toString()
    {
        return new ToStringBuilder(this)
                .append("name", name)
                .append("type", type)
                .append("format", format)
                .toString();
    }
}
