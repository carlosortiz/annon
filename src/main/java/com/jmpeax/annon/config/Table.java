package com.jmpeax.annon.config;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.Objects;
import java.util.Set;

public class Table
{
    private String name;
    private Set<Column> columns;

    public String getName()
    {
        return name;
    }

    public void setName(final String name)
    {
        this.name = name;
    }

    public Set<Column> getColumns()
    {
        return columns;
    }

    public void setColumns(final Set<Column> columns)
    {
        this.columns = columns;
    }

    @Override
    public boolean equals(final Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final Table table = (Table) o;
        return Objects.equals(name, table.name) &&
                Objects.equals(columns, table.columns);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(name, columns);
    }

    @Override
    public String toString()
    {
        return new ToStringBuilder(this)
                .append("name", name)
                .append("columns", columns)
                .toString();
    }
}
