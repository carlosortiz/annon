package com.jmpeax.annon.config;

import com.jmpeax.annon.config.AnnonConfig;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Optional;

public class Configuration
{
    private final File configFile;
    private AnnonConfig config;

    public static Configuration load(final File configFile){
        return new Configuration(configFile);
    }
    protected Configuration(final File configFile)
    {
        this.configFile = configFile;
        if (!this.configFile.exists() || !this.configFile.canRead()){
            throw new IllegalArgumentException("Config file "+configFile.getAbsolutePath()+" can be read");
        }
        final Yaml yaml = new Yaml(new Constructor(AnnonConfig.class));
        try
        {
            config = yaml.load(new FileReader(configFile));
        }
        catch (FileNotFoundException e)
        {
            config = null;
        }
        if (config==null){
            throw new IllegalStateException("Unable to load config file");
        }
    }

    public AnnonConfig getConfig()
    {
        return config;
    }
}
