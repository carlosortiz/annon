package com.jmpeax.annon.config;

public class Database
{
    private String engine;
    private String url;
    private String user;
    private String password;
    private int port;

    public Database()
    {

    }

    public String getEngine()
    {
        return engine;
    }

    public void setEngine(final String engine)
    {
        this.engine = engine;
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(final String url)
    {
        this.url = url;
    }

    public String getUser()
    {
        return user;
    }

    public void setUser(final String user)
    {
        this.user = user;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(final String password)
    {
        this.password = password;
    }

    public int getPort()
    {
        return port;
    }

    public void setPort(final int port)
    {
        this.port = port;
    }
}
