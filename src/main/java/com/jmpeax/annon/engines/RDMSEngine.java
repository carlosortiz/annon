package com.jmpeax.annon.engines;

import com.jmpeax.annon.config.Database;

import javax.sql.DataSource;

public abstract class RDMSEngine
{
    protected Database database;

    public RDMSEngine(final Database database)
    {
        this.database = database;
        if(!database.getEngine().equalsIgnoreCase(getName())){
            throw new IllegalArgumentException("Wrong DB engine implementation");
        }
    }

    public abstract String getName();
    public abstract DataSource getDataSource();
}
