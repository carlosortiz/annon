package com.jmpeax.annon;

import picocli.CommandLine;

import java.io.File;
import java.util.concurrent.Callable;


public class AnnonCLI implements Callable<Integer>
{
    @CommandLine.Option(names = {"-c", "--config"}, description = "Annon Configuration File", required = true)
    File configFile;

    @Override
    public Integer call() throws Exception
    {

        return 0;
    }
}
