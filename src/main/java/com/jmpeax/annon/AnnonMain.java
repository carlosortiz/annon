package com.jmpeax.annon;

import org.slf4j.LoggerFactory;
import picocli.CommandLine;

public class AnnonMain
{
    public static void main(String[] args)
    {
        new CommandLine(new AnnonCLI()).execute(args);
    }
}
