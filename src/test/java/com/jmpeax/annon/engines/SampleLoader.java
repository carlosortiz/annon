package com.jmpeax.annon.engines;

import com.jmpeax.annon.config.Configuration;
import com.jmpeax.annon.config.Database;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.ServiceLoader;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

class SampleLoaderTest
{

    private static File configFile;

    @BeforeAll
    static void loadFile(){
        configFile = new File(Configuration.class.getResource("/samples/sample1.yaml").getFile());
    }


    @Test
    public void testConnection() throws SQLException
    {
        final Database db = Configuration.load(configFile).getConfig().getDatabase();
        RDMSEngine h2 = new SampleRDMSImpl(db);
        final Connection conn = h2.getDataSource().getConnection();
        assertNotNull(conn);
        conn.createStatement().executeQuery("SELECT 1");
        conn.close();

    }
}
