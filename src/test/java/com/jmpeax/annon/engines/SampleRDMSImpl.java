package com.jmpeax.annon.engines;

import com.jmpeax.annon.config.Database;
import org.h2.jdbcx.JdbcDataSource;

import javax.sql.DataSource;

public class SampleRDMSImpl extends RDMSEngine
{
    public static final String SAMPLE_IMPL_NAME = "h2";

    public SampleRDMSImpl(final Database database)
    {
        super(database);
    }

    @Override
    public String getName()
    {
        return SAMPLE_IMPL_NAME;
    }

    @Override
    public DataSource getDataSource()
    {
        final JdbcDataSource dataSource = new JdbcDataSource();
        dataSource.setUrl(database.getUrl());
        dataSource.setUser(database.getUser());
        dataSource.setPassword(database.getPassword());
        return dataSource;
    }
}
