package com.jmpeax.annon;

import com.jmpeax.annon.config.AnnonConfig;
import com.jmpeax.annon.config.Configuration;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.File;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class ConfigurationTest
{

    private static File configFile;

    @BeforeAll
    static void loadFile(){
        configFile = new File(Configuration.class.getResource("/samples/sample1.yaml").getFile());
    }
    @Test
    void getConfig()
    {
        AnnonConfig annonConfig =  Configuration.load(configFile).getConfig();
        assertNotNull(annonConfig);
        assertNotNull(annonConfig.getDatabase());
        assertEquals("h2",annonConfig.getDatabase().getEngine());
        assertEquals("mem://test",annonConfig.getDatabase().getUrl());
        assertEquals("pwd",annonConfig.getDatabase().getPassword());
        assertEquals("usr",annonConfig.getDatabase().getUser());
        assertEquals(-1,annonConfig.getDatabase().getPort());

    }
}